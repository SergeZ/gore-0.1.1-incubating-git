package org.apache.gora.sql.statement;

import org.apache.avro.Schema;
import org.apache.gora.persistency.Persistent;
import org.apache.gora.sql.store.Column;
import org.apache.gora.sql.store.SqlMapping;
import org.apache.gora.sql.store.SqlStore;
import org.apache.gora.util.StringUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author Sergey Zaytsev ( comp1986@gmail.com )
 * @version 0.1
 *          <p/>
 *          Insert/Update выражение для работы с БД Oracle
 */
public class OracleSqlInsertUpdateStatement<K, V extends Persistent> extends InsertUpdateStatement<K, V> {

    public OracleSqlInsertUpdateStatement(SqlStore<K, V> store, SqlMapping mapping, String tableName) {
        super(store, mapping, tableName);
    }

    @Override
    public PreparedStatement toStatement(Connection connection) throws SQLException {
        int i = 0;
        StringBuilder builder = new StringBuilder("MERGE INTO ");
        builder.append(tableName);

        //грязный-грязный говно-хак
        String idValue = (String) ((ColumnData) columnMap.get("id")).object;

        builder.append(" dest USING ( select '").append(idValue).append("' as id from dual ) sourcee ON (dest.id = sourcee.id) WHEN NOT MATCHED THEN INSERT");
        StringUtils.join(builder.append(" ("), columnMap.keySet()).append(" )");


        SortedMap<String, ColumnData> prefixedMap = new TreeMap<String, ColumnData>();
        for (Map.Entry<String, ColumnData> e : columnMap.entrySet()) {
            String newKey = "dest." + e.getKey();
            prefixedMap.put(newKey, e.getValue());
        }

        builder.append(" VALUES (");
        for (i = 0; i < prefixedMap.size(); i++) {
            if (i != 0) builder.append(" ,");
            builder.append("?");
        }

        builder.append(")");
        builder.append(" WHERE sourcee.id IS NOT NULL WHEN MATCHED THEN UPDATE SET ");

        Column primaryColumn = mapping.getPrimaryColumn();
        Object key = prefixedMap.get("dest." + primaryColumn.getName()).object;

        i = 0;
        for (String s : prefixedMap.keySet()) {
            if (s.equals("dest." + primaryColumn.getName())) {
                continue;
            }
            if (i != 0)
                builder.append(" ,");

            builder.append(s).append("=").append("?");
            i++;
        }

        builder.append(" WHERE sourcee.id IS NOT NULL");

        PreparedStatement insert = connection.prepareStatement(builder.toString());

        int psIndex = 1;

        for(int count = 0 ; count < 2 ; count++) {
            for (Map.Entry<String, ColumnData> e : prefixedMap.entrySet()) {
                ColumnData columnData = e.getValue();
                Column column = columnData.column;
                Schema fieldSchema = columnData.schema;
                Object fieldValue = columnData.object;

                if (column.getName().equals(primaryColumn.getName())) {

                  if (count == 1) {
                    continue;
                  }
                  if (primaryColumn.getScaleOrLength() > 0) {
                    insert.setObject(psIndex++, key,
                        primaryColumn.getJdbcType().getOrder(), primaryColumn.getScaleOrLength());
                  } else {
                      insert.setObject(psIndex++, key, primaryColumn.getJdbcType().getOrder());
                  }

                 continue;
                }

                try {
                    store.setObject(insert, psIndex++, fieldValue, fieldSchema, column);
                } catch (IOException ex) {
                    throw new SQLException(ex);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }


        return insert;
    }

}
