Gora Project
============

Gora is an ORM framework for column stores such as Apache HBase and
Apache Cassandra with a specific focus on Hadoop. 

Why Gora?
---------
You can find lots of information about Gora, but this particular BitBuckets project is aimed to provide customized version of Gora � the one, which can work with Oracle database instead of only MySql and HBase as it still is. 

Why BitBucket, is not it available from official Apache Web Page ?


The simple answer is NOT � it is not a piece of cake to become an apache contributor and to share your ideas with them. This is a tremendeous work to become invited into the developer community, so I've decided to share this branch of Gora with people through BitBucket repository. Hope, this'll help to someone besides me.




How to use:

1) The project root directory contains folder named Deploy
2) Put jdbc driver from that directory into your lib directory
3) Update your gora-sql-mapping.xml file based on the one provided in Deploy directory
4) Update your gora.properties file with jdbc-url, password and user-name of your target Oracle database instance
4) Enjoy!
 


